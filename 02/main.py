import re

COLORS = ["red", "green", "blue"]

def parseLine(l):
    # ID
    id_games = l.split(":")
    id = int(re.sub("\D", "", id_games[0]))

    data = {}
    for c in COLORS:
        data[c] = 0
    
    # GAMES
    games = id_games[1].split(";")
    for g in games:
        for dice in g.split(","):
            for c in COLORS:
                if c in dice:
                    data[c] = max(data[c], int(re.sub("\D", "", dice)))
                    break
    return (id, data)


with open("input.txt") as f:
    p1 = 0
    p2 = 0
    for line in f:
        (id, data) = parseLine(line)
        if (data["red"] <= 12 and data["green"] <= 13 and data["blue"] <= 14):
            p1 += id
        p2 += data["red"] * data["green"] * data["blue"]
    print("P1 : ", p1)
    print("P2 : ", p2)