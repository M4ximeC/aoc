CARDS = {}


def init_cards(lines):
    for i in range(len(lines)):
        CARDS[i+1] = 1


def parseLine(line):
    game_numbers = line.split(":")
    numbers = game_numbers[1].split("|")
    winning_numbers = [int(wn) for wn in numbers[0].split()]
    numbers = [int(n) for n in numbers[1].split()]
    card = int(game_numbers[0].split()[-1])
    return (card, winning_numbers, numbers)


with open("input.txt") as f:
    lines = f.read().splitlines()
    init_cards(lines)
    p1 = 0
    p2 = 0
    for l_index in range(len(lines)):
        line = lines[l_index]
        (game, winning_numbers, numbers) = parseLine(line)
        sum_winning_numbers = sum(x in winning_numbers for x in numbers)
        if sum_winning_numbers != 0:
            p1 += 2**(sum_winning_numbers - 1)
            for card in range(game + 1, game + 1 + sum_winning_numbers):
                CARDS[card] += CARDS[game]
    for c in CARDS:
        p2 += CARDS[c]
    print("P1 : ", p1)
    print("P2 : ", p2)