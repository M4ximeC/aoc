HANDS = {
    "TOTAL": 0,
    "FIVE": [],
    "FOUR": [],
    "FULL": [],
    "THREE": [],
    "TWO_PAIR": [],
    "PAIR": [],
    "R" : []
}

def value(hand):
    v = 0
    cards = hand[0]
    for i in range(len(cards)):
        v += "AKQJT98765432".index(cards[i]) * (100 ** (len(cards) - i))
    return v


def parseLine(l):
    hand = l.split()
    hand[1] = int(hand[1])
    return hand


def computeHand(hand):
    cards = sorted(list(hand[0]))
    count = [1]
    current = cards[0]
    for c in cards[1:]:
        if c == current:
            count[-1] += 1
        else :
            current = c
            count.append(1)
    
    k = "R"
    if 5 in count:
        k = "FIVE"
    elif 4 in count:
        k = "FOUR"
    elif len(count) == 2:
        k = "FULL"
    elif 3 in count:
        k = "THREE"
    elif len(count) == 3:
        k = "TWO_PAIR"
    elif 2 in count:
        k = "PAIR"
    
    HANDS[k].append(hand)
    HANDS["TOTAL"] += 1

with open("input.txt") as f:
    for l in f:
        hand = parseLine(l)
        computeHand(hand)
    
    total = 0
    for k in ["FIVE", "FOUR", "FULL", "THREE", "TWO_PAIR", "PAIR", "R"]:
        HANDS[k].sort(key=value)
        for h in HANDS[k]:
            total += h[1] * HANDS["TOTAL"]
            HANDS["TOTAL"] -= 1
    
    print(total)