import math
import re
import time

now = time.time()

def getNumberRunWining(time, distance):
    DELTA = (time**2) - (4 * distance)
    x1 = (time - math.sqrt(DELTA)) / 2
    x2 = (time + math.sqrt(DELTA)) / 2 
    if int(x1) == x1:
        x1+=1
    if int(x2) == x2:
        x2-=1
    return math.floor(x2) - math.ceil(x1) + 1


with open("input.txt") as f:
    lines = f.read().splitlines()
    TIMES = [int(t) for t in lines[0].split(":")[1].split()]
    DISTANCES = [int(d) for d in lines[1].split(":")[1].split()]
    p1 = 1
    for i in range(len(TIMES)):
        p1 *= getNumberRunWining(TIMES[i], DISTANCES[i])
    print("P1 : ", p1)
    
    P2_TIME = int(re.sub('\D', '', lines[0]))
    P2_DISTANCE = int(re.sub('\D', '', lines[1]))
    print("P2 : ",getNumberRunWining(P2_TIME, P2_DISTANCE))
    print(f"{time.time() - now}s")