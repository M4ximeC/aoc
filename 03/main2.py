GEARS = {}

def searchGears(first, last, l_index, lines):
    for i in range(max(0, l_index - 1), min(len(lines), l_index + 2)):
        for j in range(max(0,first - 1), min(last + 2, len(lines[i]))):
            if (lines[i][j] == "*"):
                if not i in GEARS:
                    GEARS[i] = {}
                if not j in GEARS[i]:
                    GEARS[i][j] = []
                GEARS[i][j].append(int(lines[l_index][first:last+1]))


with open("input.txt") as f:
    lines = f.read().splitlines()
    _sum = 0
    for l_index in range(len(lines)):
        l = lines[l_index]
        i=0
        while i < len(l):
            if l[i].isdigit():
                first = i
                last = i
                while(last + 1 < len(l) and l[last+1].isdigit()):
                    last += 1
                searchGears(first, last, l_index, lines)
                i=last
            i+=1
    for k in GEARS:
        for l in GEARS[k]:
            gears = GEARS[k][l]
            if (len(GEARS[k][l]) == 2):
                _sum += gears[0] * gears[1]
    print(_sum)
