INPUT = {}


def parse_input(lines):
    current_map = ""
    for l in lines:
        if "seeds" in l:
            INPUT["seeds"] = [int(s) for s in l.split(":")[1].split()]
        elif len(l) != 0:
            if ":" in l:
                current_map = l.split()[0]
            else:
                if not current_map in INPUT:
                    INPUT[current_map] = []
                INPUT[current_map].append([int(n) for n in l.split()])


def convert(value, map_name):
    _next = None
    for l in INPUT[map_name]:
        if value >= l[1] and value < l[1] + l[2]:
            return (l[1] + l[2] - value, l[0] + value - l[1])
        elif value < l[1]:
            if not _next:
                _next = l[1]
            else:
                _next = min(_next, l[1])
    to_skip = 1
    if _next:
        to_skip = _next - value
    return (to_skip, value)


def to_location(seed):
    (s1, soil) = convert(seed, "seed-to-soil")
    (s2, fertilizer) = convert(soil, "soil-to-fertilizer")
    (s3, water) = convert(fertilizer, "fertilizer-to-water")
    (s4, light) = convert(water, "water-to-light")
    (s5, temperature) = convert(light, "light-to-temperature")
    (s6, humidity) = convert(temperature, "temperature-to-humidity")
    (s7, location) = convert(humidity, "humidity-to-location")
    return (min(s1, s2, s3, s4, s5, s6, s7), location)


with open("input.txt") as f:
    lines = f.read().splitlines()
    parse_input(lines)
    p1 = None
    for seed in INPUT["seeds"]:
        (s, loc) = to_location(seed)
        if not p1:
            p1 = loc
        else:
            p1 = min(p1, loc)
    print("P1 : ", p1)

    p2 = None
    seeds_analysed = 0
    seeds_skip = 0
    for i in range(0, len(INPUT["seeds"]), 2):
        seed = INPUT["seeds"][i]
        last_seed = INPUT["seeds"][i] + INPUT["seeds"][i+1]
        while seed < last_seed:
            (to_skip, loc) = to_location(seed)
            seeds_analysed += 1
            if not p2:
                p2 = loc
            else:
                p2 = min(p2, loc)
            if (seed + to_skip > last_seed):
                seeds_skip += last_seed - seed
            else:
                seeds_skip += to_skip
            seed += to_skip
    print("P2 : ", p2)
    print("Seeds analysed : ", seeds_analysed)
    print("Seeds skipped : ", seeds_skip)