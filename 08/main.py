import math


def parseLines(lines):
    tree = {}
    for l in lines:
        k_v = [e.strip() for e in l.split("=")]
        tree[k_v[0]] = [e.strip() for e in k_v[1][1:-1].split(",")]
    return tree


with open("input.txt") as f:
    lines = f.read().splitlines()

    INSTRUCTIONS = lines[0]
    size = len(INSTRUCTIONS)
    TREE = parseLines(lines[2:])
    
    p1 = 0
    node = 'AAA'
    while node != 'ZZZ':
        nextNode = INSTRUCTIONS[p1%size] == 'R'
        node = TREE[node][nextNode]
        p1 += 1
    print("P1 : ", p1)

    nodes = [k for k in TREE if k[-1] == 'A']
    cycles = []
    for i in range(len(nodes)):
        node = nodes[i]
        cycle = 0
        while node[-1] != 'Z':
            nextNode = INSTRUCTIONS[cycle%size] == 'R'
            node = TREE[node][nextNode]
            cycle += 1
        cycles.append(cycle)
    print("P2 : ", math.lcm(*cycles))
