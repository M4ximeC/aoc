import re

with open("input.txt") as f:
    sum = 0
    for line in f:
        numbers = re.sub('\D', '', line)
        sum += int(numbers[0] + numbers[-1])
    print(sum)