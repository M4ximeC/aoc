import re
patterns = {
    "one":"1",
    "two":"2",
    "three":"3",
    "four":"4",
    "five":"5",
    "six":"6",
    "seven":"7",
    "eight":"8",
    "nine":"9"
}
with open("input.txt") as f:
    asum = 0
    for line in f:
        for pattern in patterns:
            line = re.sub(pattern, pattern + patterns[pattern] + pattern, line)
        numbers = re.sub('\D', '', line)
        n = int(numbers[0] + numbers[-1])
        asum += n
    print(asum)